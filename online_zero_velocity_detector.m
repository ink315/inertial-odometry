%%%%在线的零速检测方法

function [Ta, Tw, accMag, gyroMag, zupt]=online_zero_velocity_detector(W, at_window, wt_window, gamma_a, gamma_w)

temp_a = 0;
temp_w = 0;
for i=1:1:W
    temp_a = temp_a +norm(at_window(:,i),2);
    temp_w = temp_w +norm(wt_window(:,i),2);
end
accMag=1/W*temp_a;
gyroMag=1/W*temp_w;

% if(accMag <gamma_a) &&(gyroMag<=gamma_w)
%     zupt = 1;
% else
%     zupt = 0;
% end

% if(accMag <gamma_a) 
%     zupt = 1;
% else
%     zupt = 0;
% end
 
   
ya_m=mean(at_window,2);
Ta=0;   

for l=1:W
    tmp=at_window(1:3,l)-ya_m;
    Ta=Ta+tmp'*tmp;    
end    

yw_m=mean(wt_window,2);
Tw=0;   

for l=1:W
    tmp=wt_window(1:3,l)-yw_m;
    Tw=Tw+tmp'*tmp;    
end    

%T=T./(sigma2_a*W);
if(Ta<1 || Tw<0.002)
    zupt = 1;
else
    zupt = 0;
end

end