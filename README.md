# inertial odometry

#### 介绍
对惯性导航的五种姿态解算方法进行了实现，并进行了对比，计算表明后四种姿态解算方法的结果是完全相同的，第一种近似方法偏差较大。

#### 设计出发点
- 在3D视觉、惯性导航、雷达里程计与建图等研究中，里程计问题 (Odometry)是对传感器（相机，IMU，雷达）位置和姿态的追踪， 是3D同步定位和建图的基础。 

- 传感器位置和姿态变化可以由多种方法检测，包括基于IMU的里 程计，基于视觉的里程计、基于雷达的里程计等。 

- 传感器位姿的表达可以划分为旋转矩阵、角轴、四元数多种表达 方式、相关论文中采用不同的表达造成阅读上难于理解。 

- 本项目总结里程计计算中传感器3D位姿表达和计算方法，可作为理解相关论文的基础

#### 项目说明
对惯性导航的五种姿态解算方法进行了实现，并进行了对比，实现了两种零速检测方法，并进行了对比。

 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/221034_c8f6d221_789731.jpeg "1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/221052_2dab8d48_789731.jpeg "2.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/221108_894eba40_789731.jpeg "3.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/221117_add082a2_789731.jpeg "4.jpg")