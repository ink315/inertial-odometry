function [xt, Rt, qt]=state_update(xt, Rt, qt, wt, dt, at, gravity, mode)

if (strcmp(mode, 'eular'))
    
    %disp('用欧拉角构成的旋转矩阵乘积更新');
    Rt=eular_update(Rt,wt, dt);

elseif (strcmp(mode, 'R1'))

    %disp('用罗德里格斯公式1更新姿态');
    Rt=dcm_update(Rt, wt, dt, 'R1');
    
elseif (strcmp(mode, 'R2'))
    %disp('用罗德里格斯公式2更新姿态');
     Rt=dcm_update(Rt, wt, dt, 'R2');
     
elseif (strcmp(mode, 'exp'))
     %disp('用指数映射更新姿态');
     Rt=dcm_update(Rt, wt, dt, 'exp');
     
elseif(strcmp(mode, 'quaternion'))

    %disp('用四元数更新姿态');
    qt=quaternion_update(qt, wt, dt);
    qt=qt/norm(qt);
    Rt = quaternion2dcm(qt);
    
end

%disp('after normalization');
 %R = normalization(R)
    
 % disp('位置速度更新');
  xt=position_update(xt, at, dt, Rt, gravity);
end


function xt=position_update(xt, at, dt, Rt, gravity)
%*************************************************************************%
% Update position and velocity states using the measured specific force,
% and the newly calculated attitude.
%*************************************************************************%

% Gravity vector
g_t=[0 0 gravity]';

% Transform the specificforce vector into navigation coordinate frame.
f_t=Rt*at;

% Subtract (add) the gravity, to obtain accelerations in navigation
% coordinat system.
acc_t=f_t+g_t;

% State space model matrices
A=eye(6);
A(1,4)=dt;
A(2,5)=dt;
A(3,6)=dt;

B=[(dt^2)/2*eye(3);dt*eye(3)];

% Update the position and velocity estimates.
xt(1:6)=A*xt(1:6)+B*acc_t;
end


function q=quaternion_update(q, wt, dt)

% References:
%
%	Titterton, D.H. and Weston, J.L. (2004). Strapdown
% Inertial Navigation Technology (2nd Ed.). Institution
% of Engineering and Technology, USA. Eq. 3.63.
%
%  	R. Gonzalez, J. Giribet, and H. Pati?o. An approach to
% benchmarking of loosely coupled low-cost navigation systems,
% Mathematical and Computer Modelling of Dynamical Systems, vol. 21,
% issue 3, pp. 272-287, 2015. Eq. 14.
%
%	Crassidis, J.L. and Junkins, J.L. (2011). Optimal Esti-
% mation of Dynamic Systems, 2nd Ed. Chapman and Hall/CRC, USA.
% Eq. 7.39, p. 458.
%
% Version: 002
% Date:    2016/11/26
% Author:  Rodrigo Gonzalez <rodralez@frm.utn.edu.ar>
% URL:     https://github.com/rodralez/navego

wnorm = norm(wt);

if wnorm < 1.E-8
    
    return;
else
    
    co = cos(0.5*wnorm*dt);
    si = sin(0.5*wnorm*dt);
    
    n1 = wt(1) / wnorm;
    n2 = wt(2) / wnorm;
    n3 = wt(3) / wnorm;
    
    qw1 = n1*si;
    qw2 = n2*si;
    qw3 = n3*si;
    qw4 = co;
    
    Om=[ qw4  qw3 -qw2 qw1;
        -qw3  qw4  qw1 qw2;
         qw2 -qw1  qw4 qw3;
        -qw1 -qw2 -qw3 qw4];
    
    q = Om * q;
end

end



function R = quaternion2dcm(q)
a = q(4); b = q(1); c = q(2); d = q(3);

R(1,1) = a*a + b*b - c*c - d*d;
R(1,2) = 2*(b*c - a*d);
R(1,3) = 2*(a*c + b*d);
R(2,1) = 2*(a*d + b*c);
R(2,2) = a*a - b*b + c*c - d*d;
R(2,3) = 2*(c*d - a*b);
R(3,1) = 2*(b*d - a*c);
R(3,2) = 2*(c*d + a*b);
R(3,3) = a*a - b*b - c*c + d*d;

end


function R=eular_update(R, w, dt)

eular = w*dt;
Rx=Rotx(eular(1));
Ry=Roty(eular(2));
Rz=Rotz(eular(3));

A=Rx*Ry*Rz;

R=R*A;
end

function Rx=Rotx(theta)

Rx=[1 0 0; 0 cos(theta) -sin(theta); 0 sin(theta) cos(theta)];

end

function Ry=Roty(theta)

Ry=[cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0  cos(theta)];

end

function Rz=Rotz(theta)

Rz=[cos(theta) -sin(theta) 0; sin(theta) cos(theta)  0; 0  0  1];

end

function S = skewm(v)

x = v(1);
y = v(2);
z = v(3);

S = [ 0 -z   y;
      z  0  -x;
     -y  x   0; ];

end

function DCMbn = dcm_update(DCMbn, wt, dt, mode)

% Reference:
%
%	Titterton, D.H. and Weston, J.L. (2004). Strapdown
% Inertial Navigation Technology (2nd Ed.). Institution
% of Engineering and Technology, USA. Eq. 11.4 and 11.10.
%
%   Groves, P.D. (2013), Principles of GNSS, Inertial, and
% Multisensor Integrated Navigation Systems (2nd Ed.). Artech House.
% 
% Version: 004
% Date:    2021/03/18
% Author:  Rodrigo Gonzalez <rodralez@frm.utn.edu.ar>
% URL:     https://github.com/rodralez/navego

euler = wt*dt;
S = skewm(euler);
mag_e = norm(euler);

if mag_e < 1.E-8
    
    if(strcmp(mode, 'R1'))
       A = eye(3);
    elseif(strcmp(mode, 'R2'))
        A = eye(3);
    elseif(strcmp(mode, 'exp'))
        A = eye(3);
    end
else
    
    % Rodrigues' formula
    % Titterton, A(k), Eq. 11.10, p. 312
    % Groves, Eq. 5.73
    %disp('罗德里格斯公式1');
    if(strcmp(mode, 'R1'))
         A = eye(3) + (sin(mag_e)/mag_e) * S + ((1-cos(mag_e))/(mag_e^2)) * S * S;
     
    % Rodrigues' formula 视觉SLAM14讲
    %disp('罗德里格斯公式2');
    elseif (strcmp(mode, 'R2'))
        n=euler/mag_e;
        S1=skewm(n);
        A= cos(mag_e)*diag(ones(1,3))+(1-cos(mag_e))*n*n'+sin(mag_e)*S1;
        
    % 指数映射. Groves, Eq. 5.69.
    %disp('指数映射');
    elseif(strcmp(mode, 'exp'))
         A = expm(S);
         
    end
end

    DCMbn = DCMbn*A;

end

function R=normalization (DCMbn)

% Brute-force orthogonalization, Groves, Eq 5.79
c1 = DCMbn(:,1);
c2 = DCMbn(:,2);
c3 = DCMbn(:,3);

c1 = c1 - 0.5 * (c1'*c2) * c2 - 0.5 * (c1'*c3) * c3 ;
c2 = c2 - 0.5 * (c1'*c2) * c1 - 0.5 * (c2'*c3) * c3 ;
c3 = c3 - 0.5 * (c1'*c3) * c1 - 0.5 * (c2'*c3) * c2 ;

% Brute-force normalization, Groves, Eq 5.80
c1 = c1 / sqrt(c1'*c1);
c2 = c2 / sqrt(c2'*c2);
c3 = c3 / sqrt(c3'*c3);

R = [c1 , c2 , c3 ];

end
